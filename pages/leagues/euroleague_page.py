from pages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


class EuroleaguePage(BasePage):
    URL_TEMPLATE = '/koszykowka/europa/euroleague/'
    _root_locator = (By.XPATH, f"//div[@class='sportName basketbal']")

    @property
    def loaded(self):
        return self.is_element_displayed(*self._root_locator)

    def get_specific_game(self, home: str, away: str, season: str):
        # todo
        'wez specyficzny mecz'
        pass

    def get_season_games(self, season: str):
        # todo
        'wez mecze z jednego sezonu'
        return self

    def get_current_season_matches(self):
        # todo
        'wez mecze z obecnego sezonu'
        pass

    def update(self):
        # todo
        'odczytanie z bazy i przeparsowanie odpwiednich dat'
        pass

    def get_all_games(self):
        # todo
        'inital'
        pass
