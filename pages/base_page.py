from pypom import Page
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from config import config
from selenium.webdriver.support.wait import WebDriverWait


class BasePage(Page):

    def __init__(self, driver, **url_kwargs):
        super().__init__(driver, **url_kwargs)
        self.wait = WebDriverWait(driver, config.MAX_WAIT)
        self.base_url = config.BASE_URL

    _root_locator = (By.XPATH, "//div[@class='sportName soccer']")

    @property
    def loaded(self):
        return self.wait.until(EC.visibility_of_element_located(self._root_locator))

    def select_section(self, section_name):
        tab_section = (By.XPATH, f"//a[@href='/{section_name}/']")
        self.wait.until(EC.visibility_of_element_located(tab_section)).click()
